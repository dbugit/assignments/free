#!/bin/bash
# shellcheck disable=SC2046,SC2068

ALL_ARGS=("$@")
ASSIGNMENT=$1
SCRIPT=$2
PARAMS=("${ALL_ARGS[@]:2}")
FILE=$(find ${ASSIGNMENT}/${SCRIPT}* -type f -executable) 
if [ -z "${FILE}" ]; then
  echo "Something went wrong, could not find the assignment, please raise a bug ticket";
  exit 1
fi
${FILE} "${PARAMS[@]}"
