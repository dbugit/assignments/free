**BMI**

Body mass index (BMI) is a value derived from the mass (weight) and height of a person. The BMI is defined as the body mass divided by the square of the body height, and is universally expressed in units of kg/m2, resulting from mass in kilograms (kg) and height in metres (m). The application reads the weight in kg and the height of a person in m using decimal point notation and outputs the calculated BMI.

* Parameter 0: height (m, decimal point notation)
* Parameter 1: weight (kg, decimal point notation)
* Expected output: BMI (kg/m2)